# Install

Required php version min 7.3.5

To install follow this steps:

- clone to yor local environment and run this commands

```bash
    cd to/project_name                  # go to your project folder  
    $ composer install                  # install dependencies  
    $ cp .env.example .env              # create .env file and copy content from .env.example
                                        # configure DB section
    $ php artisan key:generate          # generate laravel key  
    $ php artisan migrate               # run migrations
    $ php artisan db:seed               # run seeders
```

Depending on your environment - start the server.

To get response use - ```/api/v1/meals``` - endpoint
