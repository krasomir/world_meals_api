<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Meal;
use Illuminate\Http\Request;
use App\Interfaces\MealInterface;
use App\Http\Controllers\Controller;
use App\Http\Resources\MealResource;
use App\Repositories\MealRepository;
use App\Http\Requests\GetMealRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param MealInterface $mealInterface
     * @param GetMealRequest $request
     * @return ResourceCollection
     */
    public function index(MealInterface $mealInterface, GetMealRequest $request): ResourceCollection
    {
        $meals = $mealInterface->handleMealQuery($request, $request->validated());

        return MealResource::collection($meals);
    }
}
