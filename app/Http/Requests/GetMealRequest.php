<?php

namespace App\Http\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetMealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lang' => ['required', Rule::in(config('translatable.locales'))],
            'per_page' => ['sometimes', 'numeric'],
            'page' => ['sometimes', 'numeric'],
            'category' => ['sometimes', 'nullable', Rule::in(array_merge(Category::pluck('id')->toArray(),['null', '!null']))],
            'tags' => ['sometimes', 'nullable', 'array'],
            'tags.*' => ['numeric', Rule::exists('tags', 'id')],
            'with' => ['sometimes', 'nullable', 'array'],
            'with.*' => [Rule::in(['category','ingredients','tags'])],
            'diff_time' => ['sometimes', 'numeric', 'integer', 'min:1']
        ];
    }
    
    public function prepareForValidation(): void
    {
        $this->merge([
            'tags' => $this->tags ? explode(',', $this->tags) : null,
            'with' => $this->with ? explode(',', $this->with) : null,
        ]);
    }
    
    public function messages(): array
    {
        return [
            'tags.*.exists' => 'Selected tags don\'t exists.',
            'lang.in' => 'Selected language is not supported.'
        ];
    }
}
