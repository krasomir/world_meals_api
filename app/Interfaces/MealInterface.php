<?php

namespace App\Interfaces;

use Illuminate\Http\Request;


interface MealInterface
{
    public function handleMealQuery(Request $request, array $validated);
}