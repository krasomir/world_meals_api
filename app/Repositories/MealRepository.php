<?php

namespace App\Repositories;

use App\Interfaces\MealInterface;
use App\Models\Meal;
use App\Services\SearchMealService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MealRepository implements MealInterface
{
    public function handleMealQuery(Request $request, array $validated)
    {
        $query = (new SearchMealService)->search($request);

        return $query;
    }
}