<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Meal;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class SearchMealService
{
    private $request;
    private $validated;
    private $defaultPerPage = 10;

    public function search(Request $request)
    {
        $this->request = $request;
        $this->validated = $request->validated();

        $query = (new Meal)->newQuery();
        $query = $this->applyFilters($query);

        return $query;
    }

    /**
     * @param Builder $query
     * @return Illuminate\Database\Eloquent\Collection|Illuminate\Pagination\LengthAwarePaginator,
     */
    private function applyFilters($query)
    {
        $this->applyLangFilter();        
        $query = $this->applyCategoryFilter($query);
        $query = $this->applyTagsFilter($query);
        $query = $this->applyWithFilter($query);
        $query = $this->applyPageFilter($query);
        $query = $this->applyDiffTimeFilter($query);
        $query = $this->applyPerPageFilter($query);        

        return $query;
    }

    /**
     * @param Builder $query
     * @return Illuminate\Database\Eloquent\Collection|Illuminate\Pagination\LengthAwarePaginator,
     */
    private function applyPerPageFilter(Builder $query)
    {
        if ($this->request->has('per_page')) {
            $query = $query->paginate($this->validated['per_page'] ?? $this->defaultPerPage);
        } else {
            $query = $query->get();
        }

        return $query;
    }
    
    private function applyDiffTimeFilter(Builder $query): Builder
    {
        if ($this->request->has('diff_time')) {
            $diffTime = Carbon::createFromTimestamp($this->request->diff_time)->toDateTimeString();
            
            $query = $query->where(function($query) use ($diffTime) {
                $query->where('created_at', '>', $diffTime)
                    ->orWhere('updated_at', '>', $diffTime)    
                    ->orWhere('deleted_at', '>', $diffTime);
            })
            ->withTrashed();
        }

        return $query;
    }

    private function applyPageFilter(Builder $query): Builder
    {
        if ($this->request->has('page')) {
            $query = $query->limit($this->request->page)->offset($this->request->page);
        }

        return $query;
    }

    private function applyWithFilter(Builder $query): Builder
    {
        if ($this->request->has('with') && !is_null($this->validated['with'])) {
            $query = $query->with($this->request->with);
        }

        return $query;
    }

    private function applyTagsFilter(Builder $query): Builder
    { 
        if ($this->request->has('tags') && !is_null($this->validated['tags'])) {
            foreach ($this->request->tags as $tag) {
                $query = $query->whereRelation('tags', 'tag_id', $tag);
            }            
        }

        return $query;
    }

    private function applyCategoryFilter(Builder $query): Builder
    {
        if ($this->request->has('category')) {
            $this->validated['category'] = ($this->validated['category'] === 'null' || $this->validated['category'] === '!null') ? (string) $this->validated['category']: (int) $this->validated['category'];

            if (is_integer($this->validated['category'])) {
                $query = $query->whereRelation('category', 'category_id', $this->validated['category']);
            }
            if ($this->validated['category'] === 'null') {
                $query->whereNull('category_id');
            }
            if ($this->validated['category'] === '!null') {
                $query->whereNotNull('category_id');
            }
        }

        return $query;
    }

    private function applyLangFilter(): void
    {
        if ($this->request->has('lang')) {
            app()->setLocale($this->validated['lang']);
        }
    }
}