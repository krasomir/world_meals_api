<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titles = [];
        static $counter = 1;
        
        $titles = [
            'slug' => 'category-' . $counter
        ];
        foreach (config('translatable.locales') as $locale) {
            $titles[$locale] = [
                'title' => 'Naslov kategorije '. $counter .' na '. mb_strtoupper($locale) .' jeziku',
            ];
        }
        $counter++;

        return $titles;
    }
}
