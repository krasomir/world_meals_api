<?php

namespace Database\Factories;

use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Factories\Factory;

class IngredientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ingredient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titles = [];
        static $counter = 1;
        
        $titles = [
            'slug' => 'sastojak-' . $counter
        ];
        foreach (config('translatable.locales') as $locale) {
            $titles[$locale] = [
                'title' => 'Naslov sastojka '. $counter .' na '. mb_strtoupper($locale) .' jeziku',
            ];
        }
        $counter++;

        return $titles;
    }
}
