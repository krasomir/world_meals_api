<?php

namespace Database\Factories;

use App\Models\Tag;
use App\Models\Meal;
use App\Models\Category;
use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Factories\Factory;

class MealFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Meal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titles = [];
        static $counter = 1;
        $status = $this->faker->randomElement(['created', 'updated', 'deleted']);
        $createdAt = $this->faker->dateTimeThisYear(now()->subMonth(), 'Europe/Zagreb');
        
        $titles = [
            'status' => $status,
            'category_id' => $counter % 7 ? Category::inRandomOrder()->first() : null,
            'created_at' => $createdAt,
            'updated_at' => $status === 'updated' ? $this->faker->dateTimeBetween($createdAt, now()->subDays(15)) : $createdAt,
            'deleted_at' => $status === 'deleted' ? now()->subMinutes(30) : null,
        ];
        foreach (config('translatable.locales') as $locale) {
            $titles[$locale] = [
                'title' => 'Naziv jela '. $counter .' na '. mb_strtoupper($locale) .' jeziku',
                'description' => 'Opis jela '. $counter .' na '. mb_strtoupper($locale) .' jeziku',
            ];
        }
        $counter++;

        return $titles;
    }

    public function configure()
    {
        return $this->afterCreating(function (Meal $meal) {
            $ingredientIds = Ingredient::inRandomOrder()->limit(rand(1,3))->pluck('id');
            $meal->ingredients()->attach($ingredientIds);

            $tagIds = Tag::inRandomOrder()->limit(rand(1,5))->pluck('id');
            $meal->tags()->attach($tagIds);
        });
    }
}
