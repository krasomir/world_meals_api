<?php

namespace Database\Factories;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titles = [];
        static $counter = 1;
        
        $titles = [
            'slug' => 'tag-' . $counter
        ];
        foreach (config('translatable.locales') as $locale) {
            $titles[$locale] = [
                'title' => 'Naslov taga '. $counter .' na '. mb_strtoupper($locale) .' jeziku',
            ];
        }
        $counter++;

        return $titles;
    }
}
