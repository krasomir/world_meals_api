<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTranslation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('categories') && Schema::hasTable('category_translations')) {
            DB::table('categories')->truncate();
            DB::table('category_translations')->truncate();
                
            Category::factory()->count(20)->create();
        }       

        Schema::enableForeignKeyConstraints();
    }
}
