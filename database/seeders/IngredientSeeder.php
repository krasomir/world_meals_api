<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('ingredients') && Schema::hasTable('ingredient_translations')) {
            DB::table('ingredients')->truncate();
            DB::table('ingredient_translations')->truncate();
                
            Ingredient::factory()->count(20)->create();
        }       

        Schema::enableForeignKeyConstraints();
    }
}
