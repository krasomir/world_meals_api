<?php

namespace Database\Seeders;

use App\Models\Meal;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('meals') && Schema::hasTable('meal_translations') && Schema::hasTable('ingredient_meal')) {
            DB::table('meals')->truncate();
            DB::table('meal_translations')->truncate();
            DB::table('ingredient_meal')->truncate();
                
            Meal::factory()->count(60)->create();
        }       

        Schema::enableForeignKeyConstraints();
    }
}
