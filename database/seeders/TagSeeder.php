<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('tags') && Schema::hasTable('tag_translations')) {
            DB::table('tags')->truncate();
            DB::table('tag_translations')->truncate();
                
            Tag::factory()->count(20)->create();
        }       

        Schema::enableForeignKeyConstraints();
    }
}
